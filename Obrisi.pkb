create or replace NONEDITIONABLE PACKAGE BODY OBRISI AS
e_iznimka exception;
-------------------------------------------------------------------------------------------------
--Obriši OPG

  procedure p_obrisi_opg (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_opg_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_opg opg%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_opg.id,
        l_opg.deleted
    FROM 
        dual;



       begin
          update opg set
          deleted=1
       where id=l_opg.id;
          l_message := 'Uspješno ste obrisali OPG!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_opg;
----------------------------------------------------------------------------------
--Obriši adresu

  procedure p_obrisi_adresu (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_adresa_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_adresa adresa%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_adresa.id,
        l_adresa.deleted
    FROM 
        dual;



       begin
          update adresa set
          deleted=1
       where id=l_adresa.id;
          l_message := 'Uspješno ste obrisali adresu!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_adresu;
------------------------------------------------------------------------------------
--Obriši kontakt

  procedure p_obrisi_kontakt (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_kontakt_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_kontakt kontakt%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);

 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_kontakt.id,
        l_kontakt.deleted
    FROM 
        dual;



       begin
          update kontakt set
          deleted=1
       where 
          id=l_kontakt.id;
          l_message := 'Uspješno ste obrisali kontakt!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_kontakt;
---------------------------------------------------------------------------------
--Obriši vlasnika

  procedure p_obrisi_vlasnika (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_vlasnik_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_vlasnik vlasnik%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_vlasnik.id,
        l_vlasnik.deleted
    FROM 
        dual;



       begin
          update vlasnik set
          deleted=1
       where id=l_vlasnik.id;
          l_message := 'Uspješno ste obrisali vlasnika!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_vlasnika;
----------------------------------------------------------------------------------
--Obriši djelatnosti

  procedure p_obrisi_djelatnosti (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_djelatnost_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_djelatnosti djelatnosti%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_djelatnosti.id,
        l_djelatnosti.deleted
    FROM 
        dual;



       begin
          update djelatnosti set
          deleted=1
       where id=l_djelatnosti.id;
          l_message := 'Uspješno ste obrisali djelatnost!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_djelatnosti;
-------------------------------------------------------------------------------------
--Obriši zupaniju

  procedure p_obrisi_zupaniju (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_zupanije_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_zupanije zupanije%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_zupanije.id,
        l_zupanije.deleted
    FROM 
        dual;



       begin
          update zupanije set
          deleted=1
       where id=l_zupanije.id;
          l_message := 'Uspješno ste obrisali zupaniju!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_zupaniju;
------------------------------------------------------------------------------------
--Obriši zemlju

  procedure p_obrisi_zemlju (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_zemlja_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_zemlja zemlja%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_zemlja.id,
        l_zemlja.deleted
    FROM 
        dual;



       begin
          update zemlja set
          deleted=1
       where id=l_zemlja.id;
          l_message := 'Uspješno ste obrisali zemlju!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_zemlju;
------------------------------------------------------------------------------------
--Obriši kulture

  procedure p_obrisi_kulture (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_kultura_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_kulture kulture%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_kulture.id,
        l_kulture.deleted
    FROM 
        dual;



       begin
          update kulture set
          deleted=1
       where id=l_kulture.id;
          l_message := 'Uspješno ste obrisali kulturu!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_kulture;
------------------------------------------------------------------------------------
--Obriši radove

  procedure p_obrisi_radove (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_radovi_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_radove radovi%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_radove.id,
        l_radove.deleted
    FROM 
        dual;



       begin
          update radovi set
          deleted=1
       where id=l_radove.id;
          l_message := 'Uspješno ste obrisali radove!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_radove;
------------------------------------------------------------------------------------

END OBRISI;