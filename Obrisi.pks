create or replace NONEDITIONABLE PACKAGE OBRISI AS 

  procedure p_obrisi_opg(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_vlasnika(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_kontakt(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_adresu(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_djelatnosti(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_zupaniju(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_kulture(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_zemlju(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_radove(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);

END OBRISI;
