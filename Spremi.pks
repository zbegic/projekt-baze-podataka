create or replace NONEDITIONABLE PACKAGE SPREMI AS 

  procedure p_save_opg(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_vlasnik(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_kontakt (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_adresa (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);

END SPREMI;
