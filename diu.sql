--DELETE INSERT UPDATE

--Insert ID, ime, prezime, oib, pozivni broj mobitela i broj mobitela u tablicu vlasnik

INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (1, 'arukovanjski@vub.hr', 'arukovanjski1', 'Ana', 'Rukovanjski', '64600859023', '099', '7543567' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (2, 'igundulic@vub.hr', 'igundulic1', 'Ivan', 'Gundulic', '27882868274', '098', '8795262' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (3, 'pmandaric@vub.hr', 'pmandaric1', 'Petra', 'Mandaric', '72389967371', '097', '1459768' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (4, 'jmarkovic@vub.hr', 'jmarkovic1', 'Josip', 'maRko', '96352036099', '097', '6698778' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (5, 'smarulic@vub.hr', 'smarulic', 'Sanja', 'Marulic', '76213717227', '092', '7779210' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (6, 'mdjurdevic@vub.hr', 'mdjurdevic1', 'Marko', 'Djurdevic', '24514781361', '099', '2315897' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (7, 'mbekic@vub.hr', 'mbekic1', 'Marija', 'Bekic', '06895150981', '091', '7983020' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (8, 'lpergl@vub.hr', 'lpergl1', 'Laura', 'Pergl', '63063987243', '098', '4147698' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (9, 'jpetric@vub.hr', 'jpetric1', 'Josip', 'Petric', '97331164570', '092', '8823365' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (10, 'mdamjanovic@vub.hr', 'mdamjanovic1', 'Marin', 'Damjanovic', '08098032457', '099', '7602030' );
INSERT INTO Vlasnik (ID, email, lozinka, ime, prezime, oib, mobpoz, mobbr) VALUES (12, 'pglavas@vub.hr', 'pglavas1', 'Petar', 'Glavas', '21894990848', '091', '5552332' );


--Update za tablicu vlasnik

--update mijenja prezime vlasnika u markovic na ID-u 4
UPDATE Vlasnik
SET prezime = 'Markovic'
WHERE ID = 4;

--update mijenja ime vlasnika u mario na ID-u 6
UPDATE Vlasnik
SET ime = 'Mario'
WHERE ID = 6

--update mijenja ID u 11 gdje se nalazi ime petar
UPDATE Vlasnik
SET ID = '11'
WHERE ime = 'Petar'

--Brisanje podataka iz tablice vlasnik

--brise sve podatke iz tablice
TRUNCATE TABLE Vlasnik;


--brise podatke u tablici vlasnik gdje je ID 11
DELETE
FROM
    Vlasnik
WHERE
    ID = 11

--Insert ID, pozivni broj telefona, broj telefona, pozivni broj mobitela i broj mobitela u tablicu kontakt

INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (1, '035', '277978', '091', '2417787');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (2, '01', '789223', '095', '2232654');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (3, '66', '224564', '092', '8958362');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (3, '021', '668987', '0', '0');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (5, '033', '112456', '0', '0');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (6, '040', '879644', '095', '8796636');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (7, '031', '232123', '099', '1123356');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (8, '034', '331887', '098', '4456678');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (9, '052', '554222', '095', '8852266');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (10, '053', '178991', '091', '6693363');
INSERT INTO Kontakt (ID, telpoz, telbr, mobpoz, mobbr) VALUES (11, '053', '998779', '098', '2121333');

--Update za tablicu kontakt

--update mijenja pozivni broj telefona na ID-u 1
UPDATE Kontakt
SET telpoz = '032'
WHERE ID = 1;

--update mijenja pozivni broj telefona na ID-u 3
UPDATE Kontakt
SET telpoz = '021'
WHERE ID = 3

--update mijenja pozivni broj telefona na ID-u 4
UPDATE Kontakt
SET telpoz = '044'
WHERE ID = 4

--Brisanje podataka iz tablice kontakt

--brise sve podatke iz tablice
TRUNCATE TABLE Kontakt;

--brise podatke u tablici kontakt gdje je broj mobitela 2121333
DELETE
FROM
    Kontakt
WHERE
    mobbr = '2121333'

--Insert ID i naziv djelatnosti OPG-a u tablicu djelatnosti

INSERT INTO Djelatnosti (ID, djelatnost) VALUES (1, 'Proizvodnja bijelog vina');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (2, 'Obrada jaja');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (3, 'Obrada svinjskog i pile?eg mesa');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (4, 'Obrada zitarica');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (5, 'Proizovodnja mlijeka');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (6, 'Proizovdnja povrca');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (7, 'Proizvodnja voca');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (8, 'Proizvodnja sokova');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (9, 'Obrada mesa');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (10, 'Proizvodnja domaceg likera');
INSERT INTO Djelatnosti (ID, djelatnost) VALUES (11, 'Proizvodnja jaja');

--Update za tablicu djelatnosti

--update mijenja naziv djelatnosti na ID-u 7
UPDATE Djelatnosti
SET djelatnost = 'Obrada pureceg mesa'
WHERE ID = 7

--Brisanje podataka iz tablice djelatnosti

--brise sve podatke iz tablice
TRUNCATE TABLE Djelatnosti;

--brise podatke u tablici djelatnosti gdje je ID 11
DELETE
FROM
    Djelatnosti
WHERE
    ID = '11'

--Insert ID i ime zupanije u tablicu zupanije

INSERT INTO Zupanije (ID, zupanija) VALUES (1, 'Zagreba?ka �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (2, 'Krapinsko-zagorska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (3, 'Sisa?ko-moslava?ka �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (4, 'Karlova?ka �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (5, 'Vara�dinska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (6, 'Koprivni?ko-kri�eva?ka �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (7, 'Bjelovarsko-bilogorska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (8, 'Primorsko-goranska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (9, 'Li?ko-senjska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (10, 'Viroviti?ko-podravska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (11, 'Po�e�ko-slavonska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (12, 'Brodsko-posavska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (13, 'Zadarska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (14, 'Osje?ko-baranjska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (15, '�ibensko-kninska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (16, 'Vukovarsko-srijemska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (17, 'Splitsko-dalmatinska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (18, 'Istarska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (19, 'Dubrova?ko-neretvanska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (20, 'Me?imurska �upanija');
INSERT INTO Zupanije (ID, zupanija) VALUES (21, 'Grad Zagreb');
INSERT INTO Zupanije (ID, zupanija) VALUES (22, 'Istarska �upanija1');


--Brisanje podataka iz tablice zupanije

--brise sve podatke iz tablice
TRUNCATE TABLE Zupanije;

--brise podatke u tablici zupanije gdje je naziv zupanije istarska zupanija1
DELETE
FROM
    Zupanije
WHERE
    zupanija = 'Istarska �upanija1'

--Insert ID, ime opcine, mjesta i ulice te kucni broj, postanski broj i ID zupanije u tablicu adresa

INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (1, 'Bukovlje', 'Bukovlje', 'Slavonska Ulica', '66', '35209', '12');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (2, 'Ilok', 'Ilok', 'Matije Gupca', '11', '32236', '16');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (3, 'Ba�ka Voda', 'Ba�ka Voda', 'Ul. dr. fra G. Cvitanovi?a', '7', '21320', '17');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (4, 'Dvor', 'Dvor', 'Ulica Ante Brune Busica', '22', '44440', '3');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (5, 'Crnac', 'Crnac', 'Zrinska Ulica', '10', '33507', '10');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (6, 'Belica', 'Belica', 'Ulica Nikole Tesle', '122', '40319', '20');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (7, 'Cepin', 'Cepin', 'Ulica Ljudevita Gaja', '7', '31431', '14');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (8, 'Velika', 'Velika', 'Ulica Kneza Branimira', '27', '34330', '11');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (9, 'Fazana', 'Fazana', 'Ulica Matija Vlacica Flacijusa', '21', '52212', '18');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (10, 'Brinje', 'Brinje', 'Frankopanska Ulica', '50', '53260', '9');
INSERT INTO Adresa (ID, opcina, mjesto, ulica, kucnibr, postbr, IDzupanije) VALUES (11, 'Dvor1', 'Dvor1', 'Ulica Ante Brune Busica', '22', '44440', '3');

--Update za tablicu adresa

--update mijenja kucni broj na ID-u 4
UPDATE Adresa
SET kucnibr = '98'
WHERE ID = 4

--Brisanje podataka iz tablice adresa

--brise sve podatke iz tablice
TRUNCATE TABLE Adresa;

--brise podatke u tablici adresa gdje je opcina dvor1
DELETE
FROM
    Adresa
WHERE
    opcina = 'Dvor1'

--Insert ID, naziv kulture i vrstu kulture u tablicu kulture

INSERT INTO Kulture (ID, naziv, vrsta) VALUES (1, 'Vinarija', 'Gra�evina');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (2, 'Jaja', 'Koko�ja');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (3, 'Meso', 'Svinjsko i pile?e meso');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (4, 'Zitarice', 'Jecam');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (5, 'Mlijeko', 'Kravlje');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (6, 'Povrce', 'Paprika, krastavac');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (7, 'Voce', 'Svo voce');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (8, 'Sokovi', 'Domaci sok od jabuke i bazge');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (9, 'Meso', 'Purece meso');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (10, 'Liker', 'Liker od cokolade');
INSERT INTO Kulture (ID, naziv, vrsta) VALUES (10, 'Jaja', 'Pureca');

--Update za tablicu kulture

--update mijenja naziv kulture na ID-u 4
UPDATE Kulture
SET vrsta = 'Jabuka, kruska, dunja, lubenica, grozde'
WHERE ID = 7

--Brisanje podataka iz tablice kulture

--brise sve podatke iz tablice
TRUNCATE TABLE Kulture;

--brise podatke u tablici kulture gdje je naziv vrste kulture puruca
DELETE
FROM
    Kulture
WHERE
    vrsta = 'Pureca'

--Insert ID, naziv OPG-a, dostava (0 - ne 1- da), ID vlasnika, ID adrese, ID djelatnosti te ID kontakta OPG-a u tablicu OPG

INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (1, 'OPG Vinarija Ana Rukovanjski', '0', '1', '2', '1', '1');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (2, 'Obrada mesa Gundulic', '1', '2', '1', '3', '3');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (3, 'Obrada jaja Mandaric', '0', '3', '1', '2', '2');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (4, 'Obrada zitarica Markovic', '0', '4', '4', '10', '4');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (5, 'OPG proizvodnja mlijeka SM', '0', '5', '5', '9', '5');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (6, 'OPG povrce Djurdevic', '0', '6', '6', '4', '6');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (7, 'Proizvodnja voca Bekic', '0', '7', '7', '5', '7');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (8, 'Proizvodnja domacih sokova Pergl', '0', '8', '8', '6', '8');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (9, 'Obrada pureceg mesa Petric', '0', '9', '9', '7', '9');
INSERT INTO OPG (ID, naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) VALUES (10, 'Proizvodnja domaceg likera OPG Damjanovic', '0', '10', '10', '8', '10');

--Update za tablicu OPG

--update mijenja ID gdje je ID taj upisani
UPDATE OPG
SET ID = '3'
WHERE ID = 12

--update mijenja dostavu na ID-u 9
UPDATE OPG
SET dostava = '1'
WHERE ID = 9

--update mijenja dostavu na ID-u 10
UPDATE OPG
SET dostava = '1'
WHERE ID = 10

--update mijenja naziv OPG-a na ID-u 3
UPDATE OPG
SET naziv = 'Domaca jaja OPG Mandaric'
WHERE ID = 3

--update mijenja naziv OPG-a na ID-u 9
UPDATE OPG
SET naziv = 'OPG Petric meso'
WHERE ID = 9

--Brisanje podataka iz tablice OPG

--brise sve podatke iz tablice
TRUNCATE TABLE OPG;

--brise podatke u tablici OPG gdje je ID 10
DELETE
FROM
    OPG
WHERE
    ID = '10'

--Insert ID, ID OPG-a te velicinu zemlje u tablicu zemlja

INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (1, '1', '3 hektra');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (2, '3', '2 hektra');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (3, '2', '5 hektara');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (4, '4', '4 hektra');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (5, '5', '1 hektar');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (6, '6', '1 hektar');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (7, '7', '1 hektar');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (8, '8', '1 hektar');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (9, '9', '2 hektra');
INSERT INTO Zemlja (ID, IDopg, velicina) VALUES (10, '10', '1 hektar');


--Update za tablicu zemlja

--update mijenja velicinu zemlje na ID-u 5
UPDATE Zemlja
SET velicina = '2 hektra'
WHERE ID = 5

--Brisanje podataka iz tablice zemlja

--brise sve podatke iz tablice
TRUNCATE TABLE Zemlja;

--brise podatke u tablici zemlja gdje je ID 9
DELETE
FROM
    Zemlja
WHERE
    ID = '9'

--Insert ID, opis rada, ID zemlje, ID kulture te datum odradenog rada u tablicu radovi

INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (1, 'Loza spricana pesticidima radi otkljanjanja stetocina koji jedu vinovu lozu', '1', '1', '05/Svi/2020');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (2, 'Izgradnja nove hale za obradu mesa', '2', '2', '01/Ruj/2013');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (3, 'Dodavanje robota za automatsku obradu jaja', '3', '3', '21/Sij/2019');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (4, 'Spricanje radi boljeg uzgoja', '4', '4', '01/Lip/2019');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (5, 'Produzenje hale za vise mjesta za krave', '5', '5', '21/Srp/2018');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (6, 'Spricanje radi zastite od nametnika', '6', '6', '10/lip/2013');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (7, 'Spricanje radi zastite od nametnika', '7', '7', '01/Lip/2013');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (8, 'Bojanje drveta u bijelo protiv mravi', '8', '8', '01/Lip/2013');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (9, 'Izgradnja klaonice', '9', '9', '22/O�u/2020');
INSERT INTO Radovi (ID, opis, IDzemlje, IDkulture, datrad) VALUES (10, 'Izgradnja skladista za likere', '10', '10', '30/Tra/205');

--Update za tablicu radovi

--update mijenja stari ID u novi ID na ID-u 2
UPDATE Radovi
SET ID = '1'
WHERE ID = 2

--update mijenja stari ID u novi ID na ID-u 3
UPDATE Radovi
SET ID = '2'
WHERE ID = 3

--update mijenja stari ID u novi ID na ID-u 4
UPDATE Radovi
SET ID = '3'
WHERE ID = 4

--update mijenja stari ID u novi ID na ID-u 5
UPDATE Radovi
SET ID = '4'
WHERE ID = 5

--update mijenja stari ID u novi ID na ID-u 6
UPDATE Radovi
SET ID = '5'
WHERE ID = 6

--update mijenja stari ID u novi ID na ID-u 7
UPDATE Radovi
SET ID = '6'
WHERE ID = 7

--update mijenja stari ID u novi ID na ID-u 8
UPDATE Radovi
SET ID = '7'
WHERE ID = 8

--update mijenja stari ID u novi ID na ID-u 9
UPDATE Radovi
SET ID = '8'
WHERE ID = 9

--update mijenja stari ID u novi ID na ID-u 10
UPDATE Radovi
SET ID = '9'
WHERE ID = 10

--update mijenja stari ID u novi ID na ID-u 11
UPDATE Radovi
SET ID = '10'
WHERE ID = 11

--Brisanje podataka iz tablice radovi

--brise sve podatke iz tablice
TRUNCATE TABLE Radovi;

--brise podatke u tablici radovi gdje je IDzemlje 8
DELETE
FROM
    Radovi
WHERE
    IDzemlje = '8'