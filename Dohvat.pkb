create or replace NONEDITIONABLE PACKAGE BODY DOHVAT AS
e_iznimka exception;
------------------------------------------------------------------------------------
--get opg
procedure p_get_opg(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_opg json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
        SELECT 
               json_object('ID' VALUE v.ID, 
                           'OPG' VALUE o.NAZIV,
                           'VLASNIK' VALUE v.IME|| ' ' || v.PREZIME,
                           'ADRESA' VALUE a.MJESTO,
                           'ADRESA' VALUE a.ULICA,
                           'ADRESA' VALUE a.KUCNIBR,
                           'ZUPANIJE' VALUE z.ZUPANIJA,
                           'KONTAKT' VALUE k.TELPOZ,
                           'KONTAKT' VALUE k.TELBR,
                           'KONTAKT' VALUE k.MOBPOZ,
                           'KONTAKT' VALUE k.MOBBR
                            
                            ) as izlaz
             FROM
                opg o, vlasnik v, adresa a, kontakt k, zupanije z
             where
                o.deleted=0 and
                o.idvlasnik = v.id and
                o.idadresa = a.id and
                o.idkontakt = k.id and
                a.idzupanije = z.id and
                o.ID = nvl(l_id, o.ID) 
            )
        LOOP
            l_opg.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       opg
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_opg);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_opg', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_opg;
  

------------------------------------------------------------------------------------
--get adresa
procedure p_get_adresa(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_adresa json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
           SELECT 
               json_object('ID' VALUE o.ID,
                           'NAZIV' VALUE o.NAZIV,
                           'VLASNIK' VALUE v.IME || ' ' || v.PREZIME,
                           'ADRESA' VALUE a.MJESTO,
                           'ADRESA' VALUE a.ULICA,
                           'ADRESA' VALUE a.KUCNIBR,
                           'ZUPANIJE' VALUE z.ZUPANIJA
                           
                            ) as izlaz
             FROM
                opg o, vlasnik v, adresa a, zupanije z
             where
                a.deleted=0 and
                o.idvlasnik = v.id and
                o.idadresa = a.id and
                a.idzupanije = z.id and
                a.ID = nvl(l_id, a.ID) 
            )
        LOOP
            l_adresa.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       adresa
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_adresa);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_adresa', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_adresa;

------------------------------------------------------------------------------------
-- Dohvat kontakta
procedure p_get_kontakt(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_kontakt json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
        SELECT 
               json_object('ID' VALUE o.ID, 
                           'NAZIV' VALUE o.NAZIV,
                           'VLASNIK' VALUE v.IME|| ' ' || v.PREZIME,
                           'KONTAKT' VALUE k.TELPOZ,
                           'KONTAKT' VALUE k.TELBR,
                           'KONTAKT' VALUE k.MOBPOZ,
                           'KONTAKT' VALUE k.MOBBR
                           
                            ) as izlaz
             FROM
                opg o, kontakt k, vlasnik v
             where
                k.deleted=0 and
                o.idkontakt = k.id and
                o.idvlasnik = v.id and
                k.ID = nvl(l_id, k.ID) 
            )
        LOOP
            l_kontakt.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       kontakt
    where
        ID = nvl(l_id, ID) ;
            
    l_obj.put('count',l_count);
    l_obj.put('data',l_kontakt);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_kontakt', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_kontakt;
  
------------------------------------------------------------------------------------
-- Dohvat vlasnika
procedure p_get_vlasnik(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_vlasnik json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
        SELECT 
               json_object('ID' VALUE o.ID, 
                           'NAZIV' VALUE o.NAZIV,
                           'VLASNIK' VALUE v.IME|| ' ' || v.PREZIME,
                           'VLASNIK' VALUE v.MOBPOZ,
                           'VLASNIK' VALUE v.MOBBR
                           
                            ) as izlaz
             FROM
                opg o, vlasnik v
             where
                v.deleted=0 and
                o.idvlasnik = v.id and
                v.ID = nvl(l_id, v.ID) 
            )
        LOOP
            l_vlasnik.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       vlasnik
    where
        ID = nvl(l_id, ID) ;
            
    l_obj.put('count',l_count);
    l_obj.put('data',l_vlasnik);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_vlasnik', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_vlasnik;
  
------------------------------------------------------------------------------------
--login
 PROCEDURE p_login(in_json in json_object_t, out_json out json_object_t )AS
    l_obj        json_object_t := json_object_t();
    l_input      VARCHAR2(4000);
    l_record     VARCHAR2(4000);
    l_username   VLASNIK.email%TYPE;
    l_password   VLASNIK.lozinka%TYPE;
    l_id         VLASNIK.id%TYPE;
    l_out        json_array_t := json_array_t('[]');
 BEGIN
    l_obj.put('h_message', '');
    l_obj.put('h_errcode', '');
    l_input := in_json.to_string;
    SELECT
        JSON_VALUE(l_input, '$.username' RETURNING VARCHAR2),
        JSON_VALUE(l_input, '$.password' RETURNING VARCHAR2)
    INTO
        l_username,
        l_password
    FROM
        dual;

    IF (l_username IS NULL OR l_password is NULL) THEN
       l_obj.put('h_message', 'Molimo unesite korisni?ko ime i zaporku');
       l_obj.put('h_errcod', 101);
       RAISE e_iznimka;
    ELSE
       BEGIN
          SELECT
             id
          INTO 
             l_id
          FROM
             VLASNIK
          WHERE
             email = l_username AND 
             lozinka = l_password;
       EXCEPTION
             WHEN no_data_found THEN
                l_obj.put('h_message', 'Nepoznato korisni?ko ime ili zaporka');
                l_obj.put('h_errcod', 102);
                RAISE e_iznimka;
             WHEN OTHERS THEN
                RAISE;
       END;

       SELECT
          JSON_OBJECT( 
             'ID' VALUE kor.id, 
             'ime' VALUE kor.ime, 
             'prezime' VALUE kor.prezime)
       INTO 
          l_record
       FROM
          VLASNIK kor
       WHERE
          id = l_id;

    END IF;

    l_out.append(json_object_t(l_record));
    l_obj.put('data', l_out);
    out_json := l_obj;
 EXCEPTION
    WHEN e_iznimka THEN
       out_json := l_obj; 
    WHEN OTHERS THEN
       common.p_errlog('p_users_upd', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_input);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;
 END p_login;
 
 

END DOHVAT;
