--drop tablica

drop table radovi;
drop table kulture;
drop table zemlja;
drop table opg;
drop table vlasnik;
drop table djelatnosti;
drop table kontakt;
drop table adresa;
drop table zupanije;

--drop sequenci tablica

drop sequence OPG_ID_SEQ;
drop sequence VLASNIK_ID_SEQ;
drop sequence ADRESA_ID_SEQ;
drop sequence ZUPANIJE_ID_SEQ;
drop sequence KONTAKT_ID_SEQ;
drop sequence DJELATNOSTI_ID_SEQ;
drop sequence KULTURE_ID_SEQ;
drop sequence ZEMLJA_ID_SEQ;
drop sequence RADOVI_ID_SEQ;

--tablica OPG

CREATE TABLE OPG (
	ID NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(90) NOT NULL,
	dostava NUMBER(1, 0) NOT NULL,
	IDvlasnik NUMBER(9, 0) NOT NULL,
	IDadresa NUMBER(9, 0) NOT NULL,
	IDdjelatnost NUMBER(9, 0) NOT NULL,
	IDkontakt NUMBER(9, 0) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint OPG_PK PRIMARY KEY (ID));

CREATE sequence OPG_ID_SEQ;

CREATE trigger BI_OPG_ID
  before insert on OPG
  for each row
begin
  select OPG_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_OPG_UPD
  before update on OPG
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Vlasnik

CREATE TABLE Vlasnik (
	ID NUMBER(9, 0) NOT NULL,
    email VARCHAR2(30) UNIQUE NOT NULL,
    lozinka VARCHAR2(20) NOT NULL,
	ime VARCHAR2(90) NOT NULL,
	prezime VARCHAR2(90) NOT NULL,
	oib NUMBER(11, 0) NOT NULL,
	mobpoz NUMBER(3, 0) NOT NULL,
	mobbr NUMBER(7, 0) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint VLASNIK_PK PRIMARY KEY (ID));

CREATE sequence VLASNIK_ID_SEQ;

CREATE trigger BI_VLASNIK_ID
  before insert on Vlasnik
  for each row
begin
  select VLASNIK_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_VLASNIK_UPD
  before update on Vlasnik
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Adresa

CREATE TABLE Adresa (
	ID NUMBER(9, 0) NOT NULL,
	opcina VARCHAR2(90) NOT NULL,
	mjesto VARCHAR2(90) NOT NULL,
	ulica VARCHAR2(90) NOT NULL,
    kucnibr NUMBER(3, 0) NOT NULL,
	postbr NUMBER(5, 0) NOT NULL,
	IDzupanije NUMBER(9, 0) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint ADRESA_PK PRIMARY KEY (ID));

CREATE sequence ADRESA_ID_SEQ;

CREATE trigger BI_ADRESA_ID
  before insert on Adresa
  for each row
begin
  select ADRESA_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_ADRESA_UPD
  before update on Adresa
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Zupanije

CREATE TABLE Zupanije (
	ID NUMBER(9, 0) NOT NULL,
	zupanija VARCHAR2(90) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint ZUPANIJE_PK PRIMARY KEY (ID));

CREATE sequence ZUPANIJE_ID_SEQ;

CREATE trigger BI_ZUPANIJE_ID
  before insert on ZUPANIJE
  for each row
begin
  select ZUPANIJE_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_ZUPANIJE_UPD
  before update on Zupanije
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Kontakt

CREATE TABLE Kontakt (
	ID NUMBER(9, 0) NOT NULL,
	telpoz NUMBER(3, 0) NOT NULL,
	telbr NUMBER(7, 0) NOT NULL,
	mobpoz NUMBER(3, 0) NOT NULL,
	mobbr NUMBER(7, 0) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint KONTAKT_PK PRIMARY KEY (ID));

CREATE sequence KONTAKT_ID_SEQ;

CREATE trigger BI_KONTAKT_ID
  before insert on Kontakt
  for each row
begin
  select KONTAKT_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_KONTAKT_UPD
  before update on Kontakt
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Djelatnosti

CREATE TABLE Djelatnosti (
	ID NUMBER(9, 0) NOT NULL,
	djelatnost VARCHAR2(90) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint DJELATNOSTI_PK PRIMARY KEY (ID));

CREATE sequence DJELATNOSTI_ID_SEQ;

CREATE trigger BI_DJELATNOSTI_ID
  before insert on Djelatnosti
  for each row
begin
  select DJELATNOSTI_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_DJELATNOSTI_UPD
  before update on Djelatnosti
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Kulture

CREATE TABLE Kulture (
	ID NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(90) NOT NULL,
	vrsta VARCHAR2(90) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint KULTURE_PK PRIMARY KEY (ID));

CREATE sequence KULTURE_ID_SEQ;

CREATE trigger BI_KULTURE_ID
  before insert on Kulture
  for each row
begin
  select KULTURE_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_KULTURE_UPD
  before update on Kulture
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Zemlja

CREATE TABLE Zemlja (
	ID NUMBER(9, 0) NOT NULL,
	IDopg NUMBER(9, 0) NOT NULL,
	velicina VARCHAR2(40) NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint ZEMLJA_PK PRIMARY KEY (ID));

CREATE sequence ZEMLJA_ID_SEQ;

CREATE trigger BI_ZEMLJA_ID
  before insert on Zemlja
  for each row
begin
  select ZEMLJA_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_ZEMLJA_UPD
  before update on Zemlja
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica Radovi

CREATE TABLE Radovi (
	ID NUMBER(9, 0) NOT NULL,
	opis VARCHAR2(4000) NOT NULL,
	IDzemlje NUMBER(9, 0) NOT NULL,
	IDkulture NUMBER(9, 0),
	datrad DATE NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
    deleted NUMBER(1, 0) default 0 NOT NULL,
	constraint RADOVI_PK PRIMARY KEY (ID));

CREATE sequence RADOVI_ID_SEQ;

CREATE trigger BI_RADOVI_ID
  before insert on radovi
  for each row
begin
  select RADOVI_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;

/
CREATE trigger BU_RADOVI_UPD
  before update on Radovi
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--constraints

ALTER TABLE OPG ADD CONSTRAINT OPG_fk0 FOREIGN KEY (IDvlasnik) REFERENCES Vlasnik(ID);
ALTER TABLE OPG ADD CONSTRAINT OPG_fk1 FOREIGN KEY (IDadresa) REFERENCES Adresa(ID);
ALTER TABLE OPG ADD CONSTRAINT OPG_fk2 FOREIGN KEY (IDdjelatnost) REFERENCES Djelatnosti(ID);
ALTER TABLE OPG ADD CONSTRAINT OPG_fk3 FOREIGN KEY (IDkontakt) REFERENCES Kontakt(ID);

ALTER TABLE Adresa ADD CONSTRAINT Adresa_fk0 FOREIGN KEY (IDzupanije) REFERENCES Zupanije(ID);

ALTER TABLE Zemlja ADD CONSTRAINT Zemlja_fk0 FOREIGN KEY (IDopg) REFERENCES OPG(ID);

ALTER TABLE Radovi ADD CONSTRAINT radovi_fk0 FOREIGN KEY (IDzemlje) REFERENCES Zemlja(ID);
ALTER TABLE Radovi ADD CONSTRAINT radovi_fk1 FOREIGN KEY (IDkulture) REFERENCES Kulture(ID);