create or replace NONEDITIONABLE PACKAGE BODY ROUTER AS

  procedure p_main(p_in in varchar2, p_out out varchar2) AS
    l_obj JSON_OBJECT_T;
    l_procedura varchar2(40);
  BEGIN
    l_obj := JSON_OBJECT_T(p_in);

    SELECT
        JSON_VALUE(p_in, '$.procedura' RETURNING VARCHAR2)
    INTO
        l_procedura
    FROM DUAL;

    CASE l_procedura
    WHEN 'p_get_opg' THEN
        dohvat.p_get_opg(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_login' THEN
        null;
        dohvat.p_login(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_vlasnik' THEN
        dohvat.p_get_vlasnik(JSON_OBJECT_T(p_in), l_obj); 
    WHEN 'p_get_kontakt' THEN
        dohvat.p_get_kontakt(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_adresa' THEN
        dohvat.p_get_adresa(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_opg' THEN
        spremi.p_save_opg(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_vlasnik' THEN
        spremi.p_save_vlasnik(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_kontakt' THEN
        spremi.p_save_kontakt(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_adresa' THEN
        spremi.p_save_adresa(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_opg' THEN
        obrisi.p_obrisi_opg(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_vlasnika' THEN
        obrisi.p_obrisi_vlasnika(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_kontakt' THEN
        obrisi.p_obrisi_kontakt(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_adresa' THEN
        obrisi.p_obrisi_adresu(JSON_OBJECT_T(p_in), l_obj);
        
  

    ELSE
        l_obj.put('h_message', ' Nepoznata metoda ' || l_procedura);
        l_obj.put('h_errcode', 997);
    END CASE;
    p_out := l_obj.TO_STRING;
  END p_main;
END ROUTER;
