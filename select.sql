--SELECTI


--select svih podataka iz tablica

SELECT * FROM opg
SELECT * FROM vlasnik
SELECT * FROM adresa
SELECT * FROM zupanije
SELECT * FROM kontakt
SELECT * FROM djelatnosti
SELECT * FROM zemlja
select * from radovi

--select sa joinom


--select za info o OPG-u
SELECT
    o.naziv "NAZIV OPG-a",
    o.dostava,
    v.ime "IME VLASNIKA/CE",
    v.prezime "PREZIME VLASNIKA/CE",
    d.djelatnost
FROM
    OPG o
JOIN vlasnik v ON o.IDvlasnik = v.ID
JOIN adresa a ON o.IDadresa = a.ID
JOIN zupanije z ON a.IDzupanije = z.ID
JOIN djelatnosti d ON o.IDdjelatnost = d.ID


--select za adresu OPG-a
SELECT
    o.naziv "NAZIV OPG-a",
    a.mjesto,
    a.ulica,
    z.zupanija
FROM OPG o
JOIN adresa a ON o.IDadresa = a.ID
JOIN zupanije z ON a.IDzupanije = z.ID


--select za vlasnike OPG-ova
SELECT
    o.naziv "NAZIV OPG-a",
    v.ime,
    v.prezime,
    v.oib
FROM
    OPG o
JOIN vlasnik v ON o.IDvlasnik = v.ID


--select za kontakt vlasnika OPG-ova
SELECT
    o.naziv "NAZIV OPG-a",
    v.ime,
    v.prezime,
    v.mobpoz "POZIVNI BROJ MOBITELA",
    v.mobbr "BROJ MOBITELA"
FROM
    OPG o
JOIN vlasnik v ON o.IDvlasnik = v.ID

--select za kontakt OPG-ova
SELECT
    o.naziv "NAZIV OPG.a",
    k.telpoz "POZIVNI BROJ TELEFONA",
    k.telbr "TELEFONSKI BROJ",
    k.mobpoz "POZIVNI BROJ MOBITELA",
    k.mobbr "BROJ MOBITELA"
FROM
    OPG o
JOIN kontakt k ON o.IDkontakt = k.ID


--select za zemlju OPG-ova
SELECT
    z.velicina "VELICINA ZEMLJE",
    o.naziv "NAZIV OPG-a"
FROM
    Zemlja z
JOIN opg o ON z.IDopg = o.ID


--select za pregled radova na zemlji OPG-ova
SELECT
    r.opis "OPIS RADA",
    r.datrad "DATUM ODRADENOG RADA",
    z.velicina "VELICINA ZEMLJE",
    o.naziv "NAZIV OPG-a"
FROM
    Radovi r
JOIN zemlja z ON r.IDzemlje = z.ID
JOIN opg o ON z.IDopg = o.ID


--select za pregled radova i kultura
SELECT
    o.naziv "NAZIV OPG-a",
    r.opis "OPIS RADA",
    k.naziv "NAZIV KULTURE",
    k.vrsta,
    r.datrad "DATUM ODRADENOG RADA"
FROM
    Radovi r
JOIN kulture k ON r.IDkulture = k.ID
JOIN zemlja z ON r.IDzemlje = z.ID
JOIN opg o ON z.IDopg = o.ID

--8 joina

--select    