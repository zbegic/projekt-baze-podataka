create or replace NONEDITIONABLE PACKAGE BODY SPREMI AS
e_iznimka exception;
-------------------------------------------------------------------------------------------------
--Spremi opg

  procedure p_save_opg (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_opg_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_opg opg%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.naziv' ),
        JSON_VALUE(l_string, '$.dostava' ),
        JSON_VALUE(l_string, '$.IDvlasnik'),
        JSON_VALUE(l_string, '$.IDadresa' ),
        JSON_VALUE(l_string, '$.IDdjelatnost'),
        JSON_VALUE(l_string, '$.IDkontakt')
    INTO
        l_opg.id,
        l_opg.naziv,
        l_opg.dostava,
        l_opg.IDvlasnik,
        l_opg.IDadresa,
        l_opg.IDdjelatnost,
        l_opg.IDkontakt
    FROM
        dual;
  


       begin
       if nvl(l_opg.id,0)=0 then
         insert into opg(naziv, dostava, IDvlasnik, IDadresa, IDdjelatnost, IDkontakt) values (
                             l_opg.naziv, 
                             l_opg.dostava, 
                             l_opg.IDvlasnik,
                             l_opg.IDadresa,
                             l_opg.IDdjelatnost,
                             l_opg.IDkontakt);
         l_message := 'Uspješno ste unijeli opg!';
         l_errcod := 100;
        else
        update opg
        set
        naziv =l_opg.naziv,
        dostava=l_opg.dostava
        where ID=l_opg.ID;
        
        l_message := 'Uspješno ste izmijenili opg!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_opg;
-------------------------------------------------------------------------------------------------
--Spremi vlasnik

  procedure p_save_vlasnik (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_vlasnik_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_vlasnik vlasnik%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

   
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.email' ),
        JSON_VALUE(l_string, '$.lozinka' ),
        JSON_VALUE(l_string, '$.ime'),
        JSON_VALUE(l_string, '$.prezime' ),
        JSON_VALUE(l_string, '$.oib' ),
        JSON_VALUE(l_string, '$.mobpoz' ),
        JSON_VALUE(l_string, '$.mobbr' )
    INTO
        l_vlasnik.id,
        l_vlasnik.email,
        l_vlasnik.lozinka,
        l_vlasnik.ime,
        l_vlasnik.prezime,
        l_vlasnik.oib,
        l_vlasnik.mobpoz,
        l_vlasnik.mobbr
    FROM 
        dual;

    /*common.p_logiraj('test2104','ID:' || l_opg.id || 'IDteme:' || l_odabiri.IDteme || 'IDucenika:' || l_odabiri.IDucenika );   */
   
      begin
       if nvl(l_vlasnik.id,0)=0 then
         insert into vlasnik(email, lozinka, ime, prezime, oib, mobpoz, mobbr) values (
                                l_vlasnik.email,
                                l_vlasnik.lozinka,
                                l_vlasnik.ime,
                                l_vlasnik.prezime,
                                l_vlasnik.oib,
                                l_vlasnik.mobpoz,
                                l_vlasnik.mobbr);
         l_message := 'Uspješno ste unijeli vlasnika!';
         l_errcod := 100;
        else
            update vlasnik
               set
                  email=l_vlasnik.email,
                  lozinka=l_vlasnik.lozinka,
                  ime =l_vlasnik.ime,
                  prezime=l_vlasnik.prezime,
                  oib=l_vlasnik.oib,
                  mobpoz=l_vlasnik.mobpoz,
                  mobbr=l_vlasnik.mobbr
               where ID=l_vlasnik.ID;
        l_message := 'Uspješno ste izmijenili vlasnika!';
        l_errcod  := 100;            
      end if;
         
   exception
         when others then
         raise;
      end;

   
   
    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_vlasnik;
------------------------------------------------------------------------------------
--spremi kontakt
procedure p_save_kontakt (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_kontakt_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_kontakt kontakt%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.telpoz' ),
        JSON_VALUE(l_string, '$.telbr' ),
        JSON_VALUE(l_string, '$.mobpoz' ),
        JSON_VALUE(l_string, '$.mobbr' )
    INTO
        l_kontakt.id,
        l_kontakt.telpoz,
        l_kontakt.telbr,
        l_kontakt.mobpoz,
        l_kontakt.mobbr
    FROM 
        dual;



       begin
         if nvl(l_kontakt.id,0)=0 then
         insert into kontakt(telpoz, telbr, mobpoz, mobbr) values (
                                l_kontakt.telpoz,
                                l_kontakt.telbr,
                                l_kontakt.mobpoz,
                                l_kontakt.mobbr);
         l_message := 'Uspješno ste unijeli kontakt!';
         l_errcod := 100;
        else
            update kontakt set
                telpoz=l_kontakt.telpoz,
                telbr=l_kontakt.telbr,
                mobpoz=l_kontakt.mobpoz,
                mobbr=l_kontakt.mobbr
                where id=l_kontakt.id;
            l_message := 'Uspješno ste izmjenili kontakt!';
            l_errcod := 100;
        end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_kontakt;
----------------------------------------------------------------------------------
--Spremi adresa
procedure p_save_adresa (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_adresa_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_adresa adresa%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.opcina' ),
        JSON_VALUE(l_string, '$.mjesto' ),
        JSON_VALUE(l_string, '$.ulica' ),
        JSON_VALUE(l_string, '$.postbr' ),
        JSON_VALUE(l_string, '$.IDzupanije' )
    INTO
        l_adresa.id,
        l_adresa.opcina,
        l_adresa.mjesto,
        l_adresa.ulica,
        l_adresa.postbr,
        l_adresa.IDzupanije
    FROM 
        dual;
        

       begin
         if nvl(l_adresa.id,0)=0 then
         insert into adresa(opcina, mjesto, ulica, postbr, IDzupanije) values (
                                l_adresa.opcina,
                                l_adresa.mjesto,
                                l_adresa.ulica,
                                l_adresa.postbr,
                                l_adresa.IDzupanije);
         l_message := 'Uspješno ste odabrali adresu!';
         l_errcod := 100;
        else
            update adresa set
                opcina=l_adresa.opcina,
                mjesto=l_adresa.mjesto,
                ulica=l_adresa.ulica,
                postbr=l_adresa.postbr
                where id=l_adresa.id;
            l_message := 'Uspješno ste izmjenili adresa!';
            l_errcod := 100;
        end if;
       exception
          when others then
          raise;
       end;


    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       common.p_errlog('p_save_adresa', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       l_obj.put('h_message',l_message);
       l_obj.put('h_errcod',l_errcod);
       out_json := l_obj;
       ROLLBACK;    
END p_save_adresa;
-------------------------------------------------------------------------------------------------
--Spremi zupaniju

  procedure p_save_zupaniju (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_zupanija_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_zupanije zupanije%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.zupanija' )
    INTO
        l_zupanije.id,
        l_zupanije.zupanija
    FROM 
        dual;
  


       begin
       if nvl(l_zupanije.id,0)=0 then
         insert into zupanije(zupanija) values (
                             l_zupanije.zupanija);
         l_message := 'Uspješno ste unijeli zupaniju!';
         l_errcod := 100;
       else
        update zupanije
        set
        zupanija =l_zupanije.zupanija
        where ID=l_zupanije.ID;
        
        l_message := 'Uspješno ste izmijenili zupanije!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_zupaniju;
-------------------------------------------------------------------------------------------------
--Spremi djelatnosti

  procedure p_save_djelatnosti (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_djelatnost_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_djelatnosti djelatnosti%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.djelatnost' )
    INTO
        l_djelatnosti.id,
        l_djelatnosti.djelatnost
    FROM 
        dual;
  


       begin
       if nvl(l_djelatnosti.id,0)=0 then
         insert into djelatnosti(djelatnost) values (
                             l_djelatnosti.djelatnost);
         l_message := 'Uspješno ste unijeli djelatnost!';
         l_errcod := 100;
       else
        update djelatnosti 
        set
        djelatnost =l_djelatnosti.djelatnost
        where ID=l_djelatnosti.ID;
        
        l_message := 'Uspješno ste izmijenili djelatnost!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_djelatnosti;
---------------------------------------------------------------------------------
--Spremi kulture

  procedure p_save_kulture (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_kulture_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_kulture kulture%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.naziv' ),
        JSON_VALUE(l_string, '$.vrsta' )
    INTO
        l_kulture.id,
        l_kulture.naziv,
        l_kulture.vrsta
    FROM 
        dual;
  


       begin
       if nvl(l_kulture.id,0)=0 then
         insert into kulture(naziv, vrsta) values (
                             l_kulture.naziv,
                             l_kulture.vrsta);
         l_message := 'Uspješno ste unijeli kulturu!';
         l_errcod := 100;
       else
        update kulture 
        set
        naziv=l_kulture.naziv,
        vrsta=l_kulture.vrsta
        where ID=l_kulture.ID;
        
        l_message := 'Uspješno ste izmijenili kulturu!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_kulture;
-------------------------------------------------------------------------------------------------
--Spremi zemlju

  procedure p_save_zemlja (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_zemlja_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_zemlja zemlja%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IDopg' ),
        JSON_VALUE(l_string, '$.velicina' )
    INTO
        l_zemlja.id,
        l_zemlja.IDopg,
        l_zemlja.velicina
    FROM 
        dual;
  


       begin
       if nvl(l_zemlja.id,0)=0 then
         insert into zemlja(IDopg, velicina) values (
                             l_zemlja.IDopg,
                             l_zemlja.velicina);
         l_message := 'Uspješno ste unijeli zemlju!';
         l_errcod := 100;
       else
        update zemlja 
        set
        velicina=l_zemlja.velicina
        where ID=l_zemlja.ID;
        
        l_message := 'Uspješno ste izmijenili zemlju!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_zemlja;
-------------------------------------------------------------------------------------------------
--Spremi radovi

  procedure p_save_radovi (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_rad_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_radovi radovi%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.opis' ),
        JSON_VALUE(l_string, '$.IDzemlje' ),
        JSON_VALUE(l_string, '$.IDkulture' ),
        JSON_VALUE(l_string, '$.datrad' )
    INTO
        l_radovi.id,
        l_radovi.opis,
        l_radovi.IDzemlje,
        l_radovi.IDkulture,
        l_radovi.datrad
    FROM 
        dual;
  


       begin
       if nvl(l_radovi.id,0)=0 then
         insert into radovi(opis, IDzemlje, IDkulture, datrad) values (
                             l_radovi.opis,
                             l_radovi.IDzemlje,
                             l_radovi.IDkulture,
                             l_radovi.datrad);
         l_message := 'Uspješno ste unijeli rad!';
         l_errcod := 100;
       else
        update radovi 
        set
        opis=l_radovi.opis,
        datrad=l_radovi.datrad
        where ID=l_radovi.ID;
        
        l_message := 'Uspješno ste izmijenili rad!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_radovi;


END SPREMI;
